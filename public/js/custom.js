$('#command_input').bind("enterKey", function (e) {
    var parsedInput = parseInput($(this).val());
    var outputDice = [];
    var outputNumbers = [];
    for(var i = 0; i < parsedInput.dice.length; i ++){
        var diceEntry = parsedInput.dice[i];
        outputDice.push(rollMutipleDice(diceEntry.numberOfSides, diceEntry.numberOfDice, diceEntry.isExploding ));
    }

    for(var i = 0; i < parsedInput.numbers.length; i ++){
        outputNumbers.push(parsedInput.numbers[i]);
    }



    console.log(outputDice);
    addOutputBox(outputDice, outputNumbers);
    var mainContent = $("#main_content");
    mainContent.scrollTop(mainContent.prop("scrollHeight"));


});
$('#command_input').keyup(function (e) {
    if(e.keyCode == 13){
        $(this).trigger("enterKey");
    }
});

function rollMutipleDice(numberOfSides, numberOfDice, isExploding) {
    var listOfResults = [];
    for(var i = 0; i < numberOfDice; i++){
        listOfResults.push(rollDice(numberOfSides, isExploding));
    }

    return listOfResults;
}

function rollDice(numberOfSides, isExploding) {
    var output = 1 + Math.floor(Math.random() * numberOfSides);

    //To catch endlessloops exploding only for dice with more than 1 side
    while (output % numberOfSides === 0 && isExploding && numberOfSides > 1){
        output += 1 + Math.floor(Math.random() * numberOfSides);
    }

    return output;
}

function parseInput(input) {
    var splitRawInput;
    var parsedDiceOutput = [];
    var parsedNumberOutput = [];
    var finalOutput;

    //pre clean string
    input = input.replace(' ', '');

    splitRawInput = input.split('+');

    for(var i = 0; i < splitRawInput.length; i++){
        var explode = false;
        var diceEntry = {numberOfDice: "0", numberOfSides: "0", isExploding: false};
        if(splitRawInput[i].indexOf("d") >= 0){
            //check for explode and remove it
            explode = (splitRawInput[i].indexOf("!") >= 0);
            splitRawInput[i] = splitRawInput[i].replace('!', '');

            //split by d
            var temp = splitRawInput[i].split("d");

            if(temp[0] !== ""){
                diceEntry.numberOfDice = temp[0];
                diceEntry.numberOfSides = temp[1];
            } else {
                diceEntry.numberOfDice = "1";
                diceEntry.numberOfSides = temp[1];
            }

            diceEntry.isExploding = explode;

            parsedDiceOutput.push(diceEntry);
        } else {
            splitRawInput[i] = splitRawInput[i].replace(/\D/g,'');
            parsedNumberOutput.push(splitRawInput[i]);
        }
    }
    finalOutput = {dice: parsedDiceOutput, numbers: parsedNumberOutput};
    return finalOutput;
}

function addOutputBox(inputDice, inputNumber) {

    var outputString = "";
    var outputNumber = 0;
    var inputNumberSum = 0;

    for(var i = 0; i < inputDice.length ; i++){
        if(inputDice[i].length > 1) {
            outputString += (i === 0) ? "" : " + ";
            outputString += "( ";
            for(var j = 0; j < inputDice[i].length; j++){
                outputString += (j === 0) ? "" : " + ";
                outputString += inputDice[i][j];

                outputNumber += inputDice[i][j];
            }
            outputString += " )";
        } else {
            outputString += (i === 0) ? "" : " + ";
            outputString += inputDice[i][0] + " ";
            outputNumber += inputDice[i][0];
        }
    }

    for(var i = 0; i < inputNumber.length; i++){
        inputNumberSum += Number(inputNumber[i]);
    }

    outputNumber += inputNumberSum;

    var contentElement = $("<p></p>").text(outputString +" + "+ inputNumberSum + " = " + outputNumber);
    var innerElement = $("<div class=\"card-body\"></div>").append(contentElement);
    var element = $("<div class=\"card border-light\" style=\"margin: 5px\"></div>").append(innerElement);

    $('#main_content').append(element);
}